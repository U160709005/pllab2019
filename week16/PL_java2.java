import java.io.*;
import java.util.Scanner;
import java.util.*;
class PL_java2
{
  public static Map<Integer, String> FileRead(String file_name) {
    Map<Integer, String> my_dict = new HashMap<Integer, String>();
    int a = 0;
    try{
      // Open the file that is the first 
      // command line parameter
            FileInputStream fstream = new FileInputStream(file_name);
      // Get the object of DataInputStream
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
      //Read File Line By Line
        while ((strLine = br.readLine()) != null)   {
      // Print the content on the console
            my_dict.put(a, strLine);
            a++;
            }
      //Close the input stream
        in.close();
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
        return my_dict;
 }
 public static void main(String args[])
  {
   // Scanner input1 = new Scanner(System.in);  // Create a Scanner first input file
    //System.out.println("Enter your first file");
    String file_name = args[0];

    Map<Integer, String> my_dict1 = new HashMap<Integer, String>(); // Create a Hashmap first input file
    my_dict1 =FileRead(file_name);
    //System.out.println(my_dict1);
    
    //System.out.println("Enter your second file");
    file_name = args[1];                
    //input1.close();
    
    Map<Integer, String> my_dict2 = new HashMap<Integer, String>();   // Create a Hashmap first input file
    my_dict2 = FileRead(file_name);
    //System.out.println(my_dict2);
    
    System.out.println("only in the first file :\n");
    
    for(int i = 0 ; i < my_dict1.size() ; i++ ){          //comparison dict1 and dict2
      if(!my_dict2.containsValue(my_dict1.get(i))  ){
        System.out.println(my_dict1.get(i));
        
      }
    }
    
    System.out.println("only in the second file :\n");
    
    for(int i = 0 ; i < my_dict2.size() ; i++ ){    //comparison dict2 and dict1
      if(!my_dict1.containsValue(my_dict2.get(i))  ){
        System.out.println(my_dict2.get(i));
      }
    }
  }


}
